import numpy as np 
import pandas as pd
import time 
import openpyxl
import os

#swipe  date from csv 
df = pd.read_csv(r'C:\Gabinet\Modyfikacja\pacjent.csv', dtype=str, sep=';', header= 0, encoding='windows-1250', on_bad_lines='skip')

#delete unneccessery columns
df = df.drop(columns=['numer','do godziny','kategoria','pracownik','cena','ilość uczestników','czas trwania','dodatkowy czas', 'płatność online','status','potwierdzenie','wykonane','źródło','data dodania'])

# delete "WIZYTA:" from 'usługa';  "----&gt" from 'usługa' ; "48" from 'telefon'
df['usługa']= df['usługa'].str.replace('WIZYTA:','')
df['usługa']= df['usługa'].str.replace('----&gt;','')
#df['telefon']= df['telefon'].str.replace('48','')

#change column 'telfon' from str to in
#df['telefon'] = df['telefon'].astype(int) 
df['telefon'] = pd.to_numeric(df['telefon'])

#change name of columns
cols = list(df.columns.values)
cols = ['data', 'od godziny', 'klient', 'email', 'telefon', 'usługa', 'notatka']

# change place od column 'usługa'
df=df[cols]

# change all name of columns
df= df.set_axis(['Data','Godzina','Pacjent','E-mail','Telefon','Wiek','Opiekun'],axis=1, inplace=False) 

#build variable for name file
TodaysDate = time.strftime("%d-%m-%Y")
excelfilename = TodaysDate +".xlsx"


# use today date for name file
df.to_excel(excelfilename, sheet_name='sheet', index=False)


print(df)

print('Wszystko poszło zgodnie z plane. Nowy plik jest już stworzony')

